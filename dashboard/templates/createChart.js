var value1 = 120;
var value2 = 190;
var value3 = 30;
var value4 = 50;
var value5 = 20;
var value6 = 30;

function OnRedClick () {
            value1 = 121;
            myChart.data.datasets[0].data[0] += 1;
            myChart.update();
        }
function OnBlueClick () {
            value1 = 121;
            myChart.data.datasets[0].data[0] += 1;
            myChart.update();
        }
function OnYellowClick () {
            value1 = 121;
            myChart.data.datasets[0].data[0] += 1;
            myChart.update();
        }
var ctx = document.getElementById("countries");
var redButton = document.getElementById("red");
	 if (redButton.addEventListener) {   // all browsers except IE before version 9
                redButton.addEventListener ("click", OnRedClick, false);
            }
var myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [value1, value2, value3, value4, value5, value6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 2
        }]
    },
    options: {
    	legend: {
            display: true,
    	},
    	title: {
    		display: true,
    		text: 'Number of Votes'
    	}

    }
});

var cty = document.getElementById("bar");
var redButton = document.getElementById("red");
	 if (redButton.addEventListener) {   // all browsers except IE before version 9
                redButton.addEventListener ("click", OnRedClick, false);
            }
var barChart = new Chart(cty, {
    type: 'doughnut',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [value1, value2, value3, value4, value5, value6],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 2
        }]
    },
    options: {
    	legend: {
            display: true,
    	},
    	title: {
    		display: true,
    		text: 'Number of Votes'
    	}

    }
});