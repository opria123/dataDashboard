from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, JsonResponse
import mysql.connector
from django.template import Template, Context
from .models import Call

# Create your views here.

def index(request):
	dataList = Call.objects.all()
	greaterThan100 = 0
	lessThan100 = 0
	for item in dataList:
		if item.cost > 100:
			greaterThan100 = greaterThan100 + 1
		else:
			lessThan100 = lessThan100 + 1

	return render(request, 'dashboard.html', locals())
